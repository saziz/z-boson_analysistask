#ifndef AliAnalysisTaskZBoson_H
#define AliAnalysisTaskZBoson_H
#include "AliAnalysisTaskSE.h"

class TList;
class AliMuonTrackCuts;
class AliAODEvent;
class TLorentzVector;
class AliAODTrack;
class TClonesArray;
class TH1F;
class TH2F;
class TH1I;
class AliMultSelection;
class AliCounterCollection;
class THnSparse;

class AliAnalysisTaskZBoson : public AliAnalysisTaskSE {

  public:

    AliAnalysisTaskZBoson();
    AliAnalysisTaskZBoson(const char *name);
    virtual ~AliAnalysisTaskZBoson();

    virtual void NotifyRun();
    virtual void UserCreateOutputObjects();
    virtual void UserExec(Option_t *option);
    virtual void Terminate(Option_t *);
    
    /// Get track cuts
    AliMuonTrackCuts* GetTrackCuts() { return fMuonTrackCuts;}

    TLorentzVector MuonTrackToLorentzVector(const TObject *obj); //!

    
    void SetFlagUseMC(Bool_t b) {this->bIsMC = b;}
    void SetCentEstimator(TString estimator){this->centEstimator = estimator;}
    
 private:
    
    //AliAnalysisTaskZBoson(const AliAnalysisTaskZBoson&);
    // AliAnalysisTaskZBoson& operator=(const AliAnalysisTaskZBoson&);

    AliAODEvent* fAODEvent;       //! AOD event
    AliMuonTrackCuts *fMuonTrackCuts; //< Track cuts

    TString centEstimator; //
    
    TList *fOutputList; //! for histograms

    // boolean to tell whether the data is MC or not
    // DON'T FORGET THAT THIS BOOLEAN SHOULD BE STREAMED SO ONLY //
    // NOT //! 
    Bool_t bIsMC; // note that normally, we expect to run on data
    TClonesArray *mcarray; //!
    
    // some counter histograms, note that they're TH1I!
    TH1I *fEventCounterHistogram; //!
    TH1I *fMuonCounterHist; //!
    TH1I *fDimuonCounterHist; //!
    TH1I *fTriggerCounterBeforePhysSelec; //!
    TH1I *fTriggerCounterAfterPhysSelec; //!
    
    TH1F *fSingleMuonPt; //! plotting pT distribution of single muons
    THnSparseF* fZBosons; // try out THnSparse object
    TH1F *fInvMass; //! plotting invariant mass of opposite sign dimuon fourvector
    TH2F *f2DInvMassVsPtNoCuts; //!
    TH1F *fSingleMuonEta; //! eta and phi distributions of single muon tracks
    TH1F *fSingleMuonPhi; //!

    TH2I* f2DMuonChargeDist; //! we want to see per event how many mu+ and mu-'s there are
    TH1I* fZBosonFileNames; //!
    
    TH1F* fZBosonInvMass; //! basically invariant mass but with added pt>20 GeV cut
    TH1F* fZBosonInvMassLS; //! inv mass for zbosons for likesign muon pairs
    
    TH1F* fZBosonCentDist; //! centrality distribution for ZBosons
    TH1F* fZBosonCentDistLS; //! likesign muon pair cent dist
    TH1F* fZBosonY; //! same plots as for Dimuon except with mass cut on Z (60-120)
    TH1F* fZBosonPhi; //!
    TH1F* fZBosonPt; //!
    TH1F* fDimuonPhi; //!
    TH1F* fDimuonY; //! rapidity
    TH1F* fDimuonPt; //!
    TH2F* f2DMassVsPt; //!

    // fill pT and Eta of single muon tracks
    // only of muons that are candidates for z boson daughter
    TH2F* fMuPlusZBosonCandEta; //!
    TH2F* fMuMinZBosonCandEta; //!

    TH2F* fMuPlusZBosonCandPt; //!
    TH2F* fMuMinZBosonCandPt; //!

    // same as above but now for total momentum P instead of Pt
    TH2F* fMuPlusZBosonCandP; //!
    TH2F* fMuMinZBosonCandP; //!

    // define 2 separate histograms where I take the difference
    // right away
    TH2F* fMuPlusMinusMuMinPt; //!
    TH2F* fMuPlusMinusMuMinP; //!
    

    // pt distribution separately of mu+ and mu- of tracks
    // at different stages in the track selection procedure
    TH1F* fMuPlusPtAllMuons; //!
    TH1F* fMuPlusPtAfterTrackSelec; //!

    TH1F* fMuMinusPtAllMuons; //!
    TH1F* fMuMinusPtAfterTrackSelec; //!

    // centrality distribution
    TH1F* fCentDist; //!
      
    AliMultSelection *fMultSelection; //! multiplicity selection
    AliCounterCollection *fEventCounters; //! event statistics

    // some MC histograms
    // these will be generated even for nonMC files but only filled for MC
    TH1F* fMCMuPlusEta; //!
    TH1F* fMCMuPlusTheta; //!
    TH1F* fMCMuPlusPt; //!
    TH1F* fMCMuPlusP; //!
    TH1F* fMCMuMinEta; //!
    TH1F* fMCMuMinTheta; //!
    TH1F* fMCMuMinPt; //!
    TH1F* fMCMuMinP; //!
    TH1F* fMCDimuY; //!
    TH1F* fMCDimuPt; //!
    TH1F* fMCDimuP; //!
    TH1F* fMCDimuAngle; //!
    TH1F* fMCDimuInvMass; //!
    TH1F* fMCDimuInvMassBeforeCuts; //!
    TH1I* fMCGoodEventCounter; //!
    TH1I* fMCZBosonCounter; //!
    THnSparseF* fMCZBosons; //!
    THnSparseF* fMCPureZBosons; //!    

    // for acceptance x efficiency
    TH2F* fMC2DMassVsPt; //!
              
    ClassDef(AliAnalysisTaskZBoson, 1);

};

#endif
