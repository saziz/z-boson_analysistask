// ROOT includes
#include "TROOT.h"
#include "TH1.h"
#include "TH2.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TMath.h"
#include "TObjString.h"
#include "TObjArray.h"
#include "TF1.h"
#include "TList.h"
#include "TStyle.h"
#include "TString.h"
#include "TChain.h"
#include "TClonesArray.h"
#include "TH1F.h"
#include "TH1I.h"
#include "TH2F.h"
#include "THashList.h"
#include "THnSparse.h"

/// ALIROOT/STEER includes
#include "AliInputEventHandler.h"
#include "AliCentrality.h"
#include "AliAODEvent.h"
#include "AliAODTrack.h"
#include "AliAODMCParticle.h"

/// ALIROOT/ANALYSIS includes
#include "AliAnalysisManager.h"
#include "AliAnalysisTaskSE.h"
#include "AliCounterCollection.h"

// ALIPHYSICS/PWG includes
#include "AliMuonTrackCuts.h"
#include "AliAnalysisTaskZBoson.h"
#include "AliMultSelection.h"
#include "AliAnalysisMuonUtility.h"


using std::cout;
using std::endl;

Bool_t survivedCutsMCParticles(AliAODMCParticle* MCParticle);
Bool_t goodMCReco(AliAODTrack *track);

ClassImp(AliAnalysisTaskZBoson);

//__________________________________________________________________________
AliAnalysisTaskZBoson::AliAnalysisTaskZBoson() :
  AliAnalysisTaskSE(),
  fAODEvent(0x0),
  fMuonTrackCuts(0x0),
  fOutputList(0x0),
  fSingleMuonPt(0x0),
  fInvMass(0x0),
  f2DInvMassVsPtNoCuts(0x0),
  fSingleMuonEta(0x0),
  fSingleMuonPhi(0x0),
  fMultSelection(0x0),
  fZBosonInvMass(0x0),
  fZBosonInvMassLS(0x0),
  fZBosonCentDist(0x0),
  fZBosonCentDistLS(0x0),
  fZBosonY(0x0),
  fZBosonPhi(0x0),
  fZBosonPt(0x0),
  fDimuonY(0x0),
  fDimuonPhi(0x0),
  fDimuonPt(0x0),
  f2DMassVsPt(0x0),
  fEventCounters(0x0),
  fEventCounterHistogram(0x0),
  fTriggerCounterBeforePhysSelec(0x0),
  fTriggerCounterAfterPhysSelec(0x0),
  fMuonCounterHist(0x0),
  fDimuonCounterHist(0x0),
  f2DMuonChargeDist(0x0),
  fZBosonFileNames(0x0),
  fMuPlusZBosonCandEta(0x0),
  fMuMinZBosonCandEta(0x0),
  fMuPlusZBosonCandPt(0x0),
  fMuMinZBosonCandPt(0x0),
  fMuPlusZBosonCandP(0x0),
  fMuMinZBosonCandP(0x0),
  fMuPlusMinusMuMinPt(0x0),
  fMuPlusMinusMuMinP(0x0),
  fMuPlusPtAllMuons(0x0),
  fMuPlusPtAfterTrackSelec(0x0),
  fMuMinusPtAllMuons(0x0),
  fMuMinusPtAfterTrackSelec(0x0),
  fCentDist(0x0),
  fZBosons(0x0),
  fMCMuPlusEta(0x0),
  fMCMuPlusTheta(0x0),
  fMCMuPlusPt(0x0),
  fMCMuPlusP(0x0),
  fMCMuMinEta(0x0),
  fMCMuMinTheta(0x0),
  fMCMuMinPt(0x0),
  fMCMuMinP(0x0),
  fMCDimuY(0x0),
  fMCDimuPt(0x0),
  fMCDimuP(0x0),
  fMCDimuAngle(0x0),
  fMCDimuInvMass(0x0),
  fMCDimuInvMassBeforeCuts(0x0),
  fMCGoodEventCounter(0x0),
  fMCZBosonCounter(0x0),
  fMC2DMassVsPt(0x0),
  fMCZBosons(0x0),
  fMCPureZBosons(0x0)
{

}

//__________________________________________________________________________
AliAnalysisTaskZBoson::AliAnalysisTaskZBoson(const char *name) :
  AliAnalysisTaskSE(name),
  fAODEvent(0x0),
  fMuonTrackCuts(new AliMuonTrackCuts("stdMuonCuts","stdMuonCuts")),
  fOutputList(0x0),
  fSingleMuonPt(0x0),
  fInvMass(0x0),
  f2DInvMassVsPtNoCuts(0x0),
  fSingleMuonEta(0x0),
  fSingleMuonPhi(0x0),
  fMultSelection(0x0),
  fZBosonInvMass(0x0),
  fZBosonInvMassLS(0x0),
  fZBosonCentDist(0x0),
  fZBosonCentDistLS(0x0),
  fZBosonY(0x0),
  fZBosonPhi(0x0),
  fZBosonPt(0x0),
  fDimuonY(0x0),
  fDimuonPhi(0x0),
  fDimuonPt(0x0),
  f2DMassVsPt(0x0),
  fEventCounters(0x0),
  fEventCounterHistogram(0x0),
  fTriggerCounterBeforePhysSelec(0x0),
  fTriggerCounterAfterPhysSelec(0x0),
  fMuonCounterHist(0x0),
  fDimuonCounterHist(0x0),
  f2DMuonChargeDist(0x0),
  fZBosonFileNames(0x0),
  fMuPlusZBosonCandEta(0x0),
  fMuMinZBosonCandEta(0x0),
  fMuPlusZBosonCandPt(0x0),
  fMuMinZBosonCandPt(0x0),
  fMuPlusZBosonCandP(0x0),
  fMuMinZBosonCandP(0x0),
  fMuPlusMinusMuMinPt(0x0),
  fMuPlusMinusMuMinP(0x0),
  fMuPlusPtAllMuons(0x0),
  fMuPlusPtAfterTrackSelec(0x0),
  fMuMinusPtAllMuons(0x0),
  fMuMinusPtAfterTrackSelec(0x0),
  fCentDist(0x0),
  fZBosons(0x0),
  fMCMuPlusEta(0x0),
  fMCMuPlusPt(0x0),
  fMCMuPlusP(0x0),
  fMCMuMinEta(0x0),
  fMCMuMinPt(0x0),
  fMCMuMinP(0x0),
  fMCDimuY(0x0),
  fMCDimuPt(0x0),
  fMCDimuP(0x0),
  fMCDimuAngle(0x0),
  fMCDimuInvMass(0x0),
  fMCDimuInvMassBeforeCuts(0x0),
  fMCGoodEventCounter(0x0),
  fMCZBosonCounter(0x0),
  fMC2DMassVsPt(0x0),
  fMCZBosons(0x0),
  fMCPureZBosons(0x0)
{

  // Constructor
  // Define input and output slots here (never in the dummy constructor)
  // Input slot #0 works with a TChain - it is connected to the default input container
  DefineInput(0, TChain::Class());

  // Output slot #1 TList with histograms
  DefineOutput(1, TList::Class());

  fMuonTrackCuts->SetFilterMask(AliMuonTrackCuts::kMuEta | AliMuonTrackCuts::kMuThetaAbs | AliMuonTrackCuts::kMuPdca |AliMuonTrackCuts::kMuMatchLpt );
  fMuonTrackCuts->SetAllowDefaultParams(kTRUE);

}

//___________________________________________________________________________
/*AliAnalysisTaskZBoson& AliAnalysisTaskZBoson::operator=(const AliAnalysisTaskZBoson& c) 
{
  // Assignment operator
  if (this != &c) {
    AliAnalysisTaskSE::operator=(c) ;
  }
  return *this;
  }*/

//___________________________________________________________________________
/*AliAnalysisTaskZBoson::AliAnalysisTaskZBoson(const AliAnalysisTaskZBoson& c) :
  AliAnalysisTaskSE(c),
  fAODEvent(c.fAODEvent),
  fOutput(c.fOutput)
 {
  // Copy ctor
 }
*/
//___________________________________________________________________________
AliAnalysisTaskZBoson::~AliAnalysisTaskZBoson()
{
  delete fMuonTrackCuts;

  if (fOutputList) delete fOutputList;
}

//___________________________________________________________________________
void AliAnalysisTaskZBoson::NotifyRun()
{
 /// Notify run
  fMuonTrackCuts->SetRun(fInputHandler);
}


//___________________________________________________________________________
void AliAnalysisTaskZBoson::UserCreateOutputObjects(){
  // Output list
  fOutputList = new TList();
  fOutputList->SetOwner(kTRUE);
  
  // invariant mass
  // need special separate binning for this histo because we want a visible
  // jpsi and Y peak as well as Z boson

  // separately 980 bins from 1 to 60 GeV called Part1
  // then 20 bins from 60 to 140 GeV called Part2

  const Int_t nBinsTot = 3000;
  const Int_t nBinsPart1 = 2980;
  const Int_t nBinsPart2 = nBinsTot - nBinsPart1;
  
  Double_t InvMassXminPart1 = 1;
  Double_t InvMassXmaxPart1 = 60;
  Double_t binwidthPart1 = (InvMassXmaxPart1-InvMassXminPart1) / nBinsPart1;
  Double_t binwidthPart2 = (140-60) / nBinsPart2;
  Double_t InvMassXbins[nBinsTot+1];
  InvMassXbins[0] = InvMassXminPart1;
  for (Int_t i=1;i<nBinsTot+1;i++){
    if (i<nBinsPart1){
      InvMassXbins[i] = InvMassXminPart1 + i*binwidthPart1;
    }
    else {
      InvMassXbins[i] = 60 + (i-nBinsPart1) * binwidthPart2;
    }
  }

  fEventCounterHistogram = new TH1I("fEventCounterHistogram"+centEstimator, "", 3, 0, 3);
  fEventCounterHistogram->Sumw2();
  fOutputList->Add(fEventCounterHistogram);

  fTriggerCounterBeforePhysSelec = new TH1I("fTriggerCounterBeforePhysSelec"+centEstimator, "", 5, 0, 5);
  fTriggerCounterBeforePhysSelec->Sumw2();
  fOutputList->Add(fTriggerCounterBeforePhysSelec);

  fTriggerCounterAfterPhysSelec = new TH1I("fTriggerCounterAfterPhysSelec"+centEstimator, "", 5, 0, 5);
  fTriggerCounterAfterPhysSelec->Sumw2();
  fOutputList->Add(fTriggerCounterAfterPhysSelec);

  fCentDist = new TH1F("fCentDist"+centEstimator, "", 90, 0, 90);
  fCentDist->Sumw2();
  fOutputList->Add(fCentDist);
  
  fMuonCounterHist = new TH1I("fMuonCounterHist"+centEstimator, "", 3, 0, 3);
  fMuonCounterHist->Sumw2();
  fOutputList->Add(fMuonCounterHist);

  fDimuonCounterHist = new TH1I("fDimuonCounterHist"+centEstimator, "", 3, 0, 3);
  fDimuonCounterHist->Sumw2();
  fOutputList->Add(fDimuonCounterHist);

  // create the THnSparse
  Int_t thnsparseBins[4] = {90, 100, 200, 30};
  Double_t thnsparseXmin[4] = {0., 0., 0., -4.};
  Double_t thnsparseXmax[4] = {90., 250, 200., -2.5};
  fZBosons = new THnSparseF("fZBosons"+centEstimator, "", 4, thnsparseBins, thnsparseXmin, thnsparseXmax);
  fZBosons->Sumw2();
  fOutputList->Add(fZBosons);
  
  fInvMass = new TH1F("fInvMass"+centEstimator, "", nBinsTot, InvMassXbins);
  fInvMass->Sumw2();
  fOutputList->Add(fInvMass);

  f2DInvMassVsPtNoCuts = new TH2F("f2DInvMassVsPtNoCuts"+centEstimator, "", 200, 0, 200, nBinsTot, InvMassXbins);
  f2DInvMassVsPtNoCuts->Sumw2();
  fOutputList->Add(f2DInvMassVsPtNoCuts);
  
  // single muon pT distribution
  fSingleMuonPt = new TH1F("fSingleMuonPt"+centEstimator, "", 120, 0, 120);
  fSingleMuonPt->Sumw2();
  fOutputList->Add(fSingleMuonPt);

  // single muon eta distribution
  fSingleMuonEta = new TH1F("fSingleMuonEta"+centEstimator, "", 100, -4, -2.5);
  fSingleMuonEta->Sumw2();
  fOutputList->Add(fSingleMuonEta);

  // single muon phi distribution
  fSingleMuonPhi = new TH1F("fSingleMuonPhi"+centEstimator, "", 100, 0, 2*TMath::Pi());
  fSingleMuonPhi->Sumw2();
  fOutputList->Add(fSingleMuonPhi);

  // invariant mass distribution for Z bosons, which is basically inv mass for muons with pt>20
  // #bins, and corresponding range is chosen such that bin width is 2.5 GeV/c^2
  // original range 56 bins from 0 to 140
  fZBosonInvMass = new TH1F("fZBosonInvMass"+centEstimator, "", 200, 0, 500);
  fZBosonInvMass->Sumw2();
  fOutputList->Add(fZBosonInvMass);

  // invariant mass distribution for Z bosons, which is basically inv mass for muons with pt>20
  // this time for like sign muon pairs
  fZBosonInvMassLS = new TH1F("fZBosonInvMassLS"+centEstimator, "", 200, 0, 500);
  fZBosonInvMassLS->Sumw2();
  fOutputList->Add(fZBosonInvMassLS);

  // centrality distribution for unlike sign muon pairs
  fZBosonCentDist = new TH1F("fZBosonCentDist"+centEstimator, "", 20, 0, 100);
  fZBosonCentDist->Sumw2();
  fOutputList->Add(fZBosonCentDist);

  // cent dis for likesign muon pairs
  fZBosonCentDistLS = new TH1F("fZBosonCentDistLS"+centEstimator, "", 20, 0, 100);
  fZBosonCentDistLS->Sumw2();
  fOutputList->Add(fZBosonCentDistLS);

  // rapidity distribution
  fZBosonY = new TH1F("fZBosonY"+centEstimator, "", 30, -4., -2.5);
  fZBosonY->Sumw2();
  fOutputList->Add(fZBosonY);

  // dimuon phi distribution
  fZBosonPhi = new TH1F("fZBosonPhi"+centEstimator, "", 100, 0, 2*TMath::Pi());
  fZBosonPhi->Sumw2();
  fOutputList->Add(fZBosonPhi);

  // dimuon pt distribution
  fZBosonPt = new TH1F("fZBosonPt"+centEstimator, "", 1000, 0., 1000.);
  fZBosonPt->Sumw2();
  fOutputList->Add(fZBosonPt);
  
  // rapidity distribution
  fDimuonY = new TH1F("fDimuonY"+centEstimator, "", 30, -4, -2.5);
  fDimuonY->Sumw2();
  fOutputList->Add(fDimuonY);

  // dimuon phi distribution
  fDimuonPhi = new TH1F("fDimuonPhi"+centEstimator, "", 100, 0, 2*TMath::Pi());
  fDimuonPhi->Sumw2();
  fOutputList->Add(fDimuonPhi);

  // dimuon pt distribution
  fDimuonPt = new TH1F("fDimuonPt"+centEstimator, "", 1000, 0., 1000.);
  fDimuonPt->Sumw2();
  fOutputList->Add(fDimuonPt);

  f2DMuonChargeDist = new TH2I("f2DMuonChargeDist"+centEstimator, "", 5, 0, 5, 5, 0, 5);
  f2DMuonChargeDist->Sumw2();
  f2DMuonChargeDist->SetTitle(";mu+;mu-");
  fOutputList->Add(f2DMuonChargeDist);

  fZBosonFileNames = new TH1I("fZBosonFileNames"+centEstimator, "", 300, 0, 300);
  fOutputList->Add(fZBosonFileNames);

  // 2d distribution for mass vs pt
  f2DMassVsPt = new TH2F("f2DMassVsPt"+centEstimator, "", 1000, 0, 1000, 200, 0, 500);
  f2DMassVsPt->Sumw2();
  fOutputList->Add(f2DMassVsPt);

fMuPlusZBosonCandEta = new TH2F("fMuPlusZBosonCandEta"+centEstimator, "", 30, -4, -2.5, 200, 0, 500);
  fMuPlusZBosonCandEta->Sumw2();
  fOutputList->Add(fMuPlusZBosonCandEta);

  fMuMinZBosonCandEta = new TH2F("fMuMinZBosonCandEta"+centEstimator, "", 30, -4, -2.5, 200, 0, 500);
  fMuMinZBosonCandEta->Sumw2();
  fOutputList->Add(fMuMinZBosonCandEta);

  fMuPlusZBosonCandPt = new TH2F("fMuPlusZBosonCandPt"+centEstimator, "", 500, 0, 500., 200, 0, 500);
  fMuPlusZBosonCandPt->Sumw2();
  fOutputList->Add(fMuPlusZBosonCandPt);

  fMuMinZBosonCandPt = new TH2F("fMuMinZBosonCandPt"+centEstimator, "", 500, 0, 500., 200, 0, 500);
  fMuMinZBosonCandPt->Sumw2();
  fOutputList->Add(fMuMinZBosonCandPt);

  fMuPlusZBosonCandP = new TH2F("fMuPlusZBosonCandP"+centEstimator, "", 1200, 0, 1200., 200, 0, 500);
  fMuPlusZBosonCandP->Sumw2();
  fOutputList->Add(fMuPlusZBosonCandP);

  fMuMinZBosonCandP = new TH2F("fMuMinZBosonCandP"+centEstimator, "", 1200, 0, 1200., 200, 0, 500);
  fMuMinZBosonCandP->Sumw2();
  fOutputList->Add(fMuMinZBosonCandP);

  fMuPlusMinusMuMinPt = new TH2F("fMuPlusMinusMuMinPt"+centEstimator, "", 400, -200, 200, 200, 0, 500);
  fMuPlusMinusMuMinPt->Sumw2();
  fOutputList->Add(fMuPlusMinusMuMinPt);

  fMuPlusMinusMuMinP = new TH2F("fMuPlusMinusMuMinP"+centEstimator, "", 800, -400, 400, 200, 0, 500);
  fMuPlusMinusMuMinP->Sumw2();
  fOutputList->Add(fMuPlusMinusMuMinP);

  fMuPlusPtAllMuons = new TH1F("fMuPlusPtAllMuons"+centEstimator, "", 500, 0, 500);
  fMuPlusPtAllMuons->Sumw2();
  fOutputList->Add(fMuPlusPtAllMuons);

  fMuPlusPtAfterTrackSelec = new TH1F("fMuPlusPtAfterTrackSelec"+centEstimator, "", 500, 0, 500);
  fMuPlusPtAfterTrackSelec->Sumw2();
  fOutputList->Add(fMuPlusPtAfterTrackSelec);

  fMuMinusPtAllMuons = new TH1F("fMuMinusPtAllMuons"+centEstimator, "", 500, 0, 500);
  fMuMinusPtAllMuons->Sumw2();
  fOutputList->Add(fMuMinusPtAllMuons);
  
  fMuMinusPtAfterTrackSelec = new TH1F("fMuMinusPtAfterTrackSelec"+centEstimator, "", 500, 0, 500);
  fMuMinusPtAfterTrackSelec->Sumw2();
  fOutputList->Add(fMuMinusPtAfterTrackSelec);
  
  // initialize event counter
  fEventCounters = new AliCounterCollection("eventCounters"+centEstimator);
  fEventCounters->AddRubric("trigger",100);
  fEventCounters->AddRubric("run",1000);
  fEventCounters->AddRubric("selected","yes/no");
  fEventCounters->Init();
  fOutputList->Add(fEventCounters);

  if(bIsMC){
    ///////////////////////////// MC //////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    
    // now all the MC histograms follow
    
    //first off a counter to see how many 'good' events we have,
    //namely those with a Z boson with inv mass between 60-120 GeV
    fMCGoodEventCounter = new TH1I("fMCGoodEventCounter"+centEstimator, "", 4, 0, 4);
    fMCGoodEventCounter->Sumw2();
    fOutputList->Add(fMCGoodEventCounter);
    
    fMCZBosonCounter = new TH1I("fMCZBosonCounter"+centEstimator, "", 5, 0, 5);
    fMCZBosonCounter->Sumw2();
    fOutputList->Add(fMCZBosonCounter);
    
    fMCMuPlusEta = new TH1F("fMCMuPlusEta"+centEstimator, "", 30, -4., -2.5);
    fMCMuPlusEta->Sumw2();
    fOutputList->Add(fMCMuPlusEta);
    
    fMCMuPlusTheta = new TH1F("fMCMuPlusTheta"+centEstimator, "", 180, 0., 180.);
    fMCMuPlusTheta->Sumw2();
    fOutputList->Add(fMCMuPlusTheta);
    
    fMCMuPlusPt = new TH1F("fMCMuPlusPt"+centEstimator, "", 500, 0, 500);
    fMCMuPlusPt->Sumw2();
    fOutputList->Add(fMCMuPlusPt);
    
    fMCMuPlusP = new TH1F("fMCMuPlusP"+centEstimator, "", 120, 0, 1200);
    fMCMuPlusP->Sumw2();
    fOutputList->Add(fMCMuPlusP);
    
    fMCMuMinEta = new TH1F("fMCMuMinEta"+centEstimator, "", 30, -4., -2.5);
    fMCMuMinEta->Sumw2();
    fOutputList->Add(fMCMuMinEta);
    
    fMCMuMinTheta = new TH1F("fMCMuMinTheta"+centEstimator, "", 180, 0., 180.);
    fMCMuMinTheta->Sumw2();
    fOutputList->Add(fMCMuMinTheta);
    
    fMCMuMinPt = new TH1F("fMCMuMinPt"+centEstimator, "", 500, 0, 500);
    fMCMuMinPt->Sumw2();
    fOutputList->Add(fMCMuMinPt);
    
    fMCMuMinP = new TH1F("fMCMuMinP"+centEstimator, "", 120, 0, 1200);
    fMCMuMinP->Sumw2();
    fOutputList->Add(fMCMuMinP);
    
    fMCDimuY = new TH1F("fMCDimuY"+centEstimator, "", 30, -4., -2.5);
    fMCDimuY->Sumw2();
    fOutputList->Add(fMCDimuY);
    
    fMCDimuPt = new TH1F("fMCDimuPt"+centEstimator, "", 500, 0, 500);
    fMCDimuPt->Sumw2();
    fOutputList->Add(fMCDimuPt);
    
    fMCDimuP = new TH1F("fMCDimuP"+centEstimator, "", 120, 0, 1200);
    fMCDimuP->Sumw2();
    fOutputList->Add(fMCDimuP);
    
    fMCDimuAngle = new TH1F("fMCDimuAngle"+centEstimator, "", 80, 0, 2*TMath::Pi());
    fMCDimuAngle->Sumw2();
    fOutputList->Add(fMCDimuAngle);
    
    fMCDimuInvMass = new TH1F("fMCDimuInvMass"+centEstimator, "", 112, 0, 280);
    fMCDimuInvMass->Sumw2();
    fOutputList->Add(fMCDimuInvMass);
    
    fMCDimuInvMassBeforeCuts = new TH1F("fMCDimuInvMassBeforeCuts"+centEstimator, "", 112, 0, 280);
    fMCDimuInvMassBeforeCuts->Sumw2();
    fOutputList->Add(fMCDimuInvMassBeforeCuts);

    // 2d distribution for mass vs pt for n generated events
    fMC2DMassVsPt = new TH2F("fMC2DMassVsPt"+centEstimator, "", 1000, 0, 1000, 200, 0, 500);
    fMC2DMassVsPt->Sumw2();
    fOutputList->Add(fMC2DMassVsPt);
    
    // note that I'm creating the MC THnSparse with the exact same binning as the data one!
    fMCZBosons = new THnSparseF("fMCZBosons"+centEstimator, "", 4, thnsparseBins, thnsparseXmin, thnsparseXmax);
    fMCZBosons->Sumw2();
    fOutputList->Add(fMCZBosons);

    // this one will take properties directly from the Z boson mother instead of the dimuon pair
    fMCPureZBosons = new THnSparseF("fMCPureZBosons"+centEstimator, "", 4, thnsparseBins, thnsparseXmin, thnsparseXmax);
    fMCPureZBosons->Sumw2();
    fOutputList->Add(fMCPureZBosons);
  }

  
  
  // Required both here and in UserExec()
  PostData(1, fOutputList);
}

//___________________________________________________________________________
void AliAnalysisTaskZBoson::UserExec(Option_t *)
{
  // Execute analysis for current InputEvent
  fAODEvent = dynamic_cast<AliAODEvent*> (InputEvent());
  if ( ! fAODEvent ) {
    AliError ("AOD event not found. Nothing done!");
    return;
  }  
  
  // Physics selection
  UInt_t IsSelected = (((AliInputEventHandler*)(AliAnalysisManager::GetAnalysisManager()->GetInputEventHandler()))->IsEventSelected());
  
  TString trigClass = fAODEvent->GetFiredTriggerClasses();
  // fill the appropriate trigger counting histogram
  if (trigClass.Contains("CINT7-B-NOPF-MUFAST")){
    fTriggerCounterBeforePhysSelec->Fill("CINT7", 1);
    if(IsSelected & AliVEvent::kINT7inMUON){
      fTriggerCounterAfterPhysSelec->Fill("CINT7", 1);
    }
  }

  if (trigClass.Contains("CMUL7-B-NOPF-MUFAST")){
    fTriggerCounterBeforePhysSelec->Fill("CMUL7", 1);
    fEventCounterHistogram->Fill("trig", 1);
    if(IsSelected & AliVEvent::kMuonUnlikeLowPt7){
      fTriggerCounterAfterPhysSelec->Fill("CMUL7", 1);
    }
  }
  
  if (trigClass.Contains("CMSL7-B-NOPF-MUFAST")){
    fTriggerCounterBeforePhysSelec->Fill("CMSL7", 1);
    if(IsSelected & AliVEvent::kMuonSingleLowPt7){
      fTriggerCounterAfterPhysSelec->Fill("CMSL7", 1);
    }
  }
  
  if (trigClass.Contains("CMSH7-B-NOPF-MUFAST")){
    fTriggerCounterBeforePhysSelec->Fill("CMSH7", 1);
    if(IsSelected & AliVEvent::kMuonSingleHighPt7){  
      fTriggerCounterAfterPhysSelec->Fill("CMSH7", 1);
    }
  }

  // if MC, use CINT7 and appropriate phys selection mask
  if (bIsMC){
    if (!(trigClass.Contains("CINT7-B-NOPF-MUFAST") && (IsSelected & AliVEvent::kINT7inMUON))){
      return;
    } 
  } // otherwise assume regular data and run on CMUL7
  else {
    if (!(trigClass.Contains("CMUL7-B-NOPF-MUFAST") && (IsSelected & AliVEvent::kMuonUnlikeLowPt7))){
      return;
    }
  }
  
  fEventCounterHistogram->Fill("trig+phys", 1);

  // Multiplicity selection
  Float_t centrality = -10;
  
  AliMultSelection *fMultSelection =static_cast<AliMultSelection*>(fAODEvent->FindListObject("MultSelection"));
  if (!fMultSelection) {
    AliError ("No multiplicity/centrality information available!. Nothing done");
    return;
  }
  
  centrality = fMultSelection->GetMultiplicityPercentile(centEstimator);

  // for event counting
  fCentDist->Fill(centrality);
  
  // we only want events in 0-90 centrality percentile
  if(centrality < 0. || centrality > 90.) {
    //cout << "no good value for centrality: " << centrality << endl;
    return;
  }

  fEventCounterHistogram->Fill("+ also centrality selection", 1);
  
  //
  // now we start (di)muon loop
  //

  Int_t nTracks = 0; // obsolete for now, as I switched over to MuonUtility
  Int_t nTracksMuonUtil = 0;
  nTracksMuonUtil = AliAnalysisMuonUtility::GetNTracks(fAODEvent);
  // the indexes will be used for MC reco to check if both muons came from same Z
  Int_t indexMother1 = -1;
  Int_t indexMother2 = -1;
  nTracks = fAODEvent->GetNumberOfTracks();
  Int_t muonCharge = 1; // charge of a positive muon
  if (bIsMC) {muonCharge=3;} // when running the smearing task on MC, all charges are changed
  // in terms of |e| to |e|/3 so muon charge becomes 3!
  
  
  // this will be the number of mu+ and mu- that pass track quality as well as pT>20!
  Int_t numMuPlus = 0;
  Int_t numMuMin = 0;
  
  //first track loop
  for (Int_t iTrack = 0; iTrack < nTracksMuonUtil; iTrack++) {
    AliVParticle *track1 = (AliVParticle*)AliAnalysisMuonUtility::GetTrack(iTrack,fAODEvent);

    if (!track1) {
      AliError(Form("ERROR: Could not retrieve track %d", iTrack));
      continue;
    }

    // fill the counter as well as pt distributions before track quality cuts
    fMuonCounterHist->Fill("all muon tracks", 1);
    if (track1->Charge()==muonCharge){
      fMuPlusPtAllMuons->Fill(track1->Pt());
    }
    else if (track1->Charge()==-muonCharge){
      fMuMinusPtAllMuons->Fill(track1->Pt());
    }
    

    //some basic checks
    //offline trigger (in LHC15o, Apt = 0.5 GeV/c, Lpt = 1 GeV/c, Hpt = 4 GeV/c)
    // matchTrig>=1: Apt 
    // matchTrig>=2: Lpt
    // matchTrig>=3: Hpt
    //Int_t matchTrig = track1->GetMatchTrigger();
    //Bool_t isMuon = track1->IsMuonTrack();
    Float_t etaMuon = track1->Eta();
    Float_t ptMuon = track1->Pt();

    // simple way to select only muon track with standard muon cuts applied as well
    if (!fMuonTrackCuts->IsSelected(track1)) continue;
    // also check that the particle came from a good MC particle
    // for the definition of good, check the goodMCReco function

    // for the MC reco, we want specific muon tracks that come from the MC truth
    if (bIsMC){
      // check if the particle is associated to an MC particle
      // if the track  has no associated particle, throw it away!
      if (track1->GetLabel() == -1) continue;

      // only if it did, check if the mother MC particle was a Z
      TClonesArray *mcarray =
	dynamic_cast<TClonesArray*>(InputEvent()->FindListObject(AliAODMCParticle::StdBranchName()));
      if (mcarray){	
	AliAODMCParticle* mcp = (AliAODMCParticle*)mcarray->At(track1->GetLabel());

	// check that the muon has a correct status code of 1
	// if not, throw it away!
	if (mcp->MCStatusCode() != 1) continue;

	// check that the muon has a mother in the first place and if so, it is a Z
	// if not, throw it away!
	indexMother1 = mcp->GetMother();
	if (indexMother1 == -1) continue;
	// now test if mother/grandmother/GGmother/etc was a Z
	Bool_t cameFromZ1 = kFALSE;
	while (indexMother1 > -1){
	  AliAODMCParticle* motherMcp = (AliAODMCParticle*)mcarray->At(indexMother1);
	  if (motherMcp->PdgCode() == 23){
	    cameFromZ1 = kTRUE;
	    indexMother1 = -5; // ugly solution!
	  }
	  else{
	    indexMother1 = motherMcp->GetMother();
	  }
	}
	if (!cameFromZ1) continue;
      }
    }

    // again we want to know single muon Pt distributions, this time after
    // applying track quality cuts
    if (track1->Charge()==muonCharge){
      fMuPlusPtAfterTrackSelec->Fill(track1->Pt());
    }
    else if (track1->Charge()==-muonCharge){
      fMuMinusPtAfterTrackSelec->Fill(track1->Pt());
    }

    // for muon track counters
    fMuonCounterHist->Fill("muon tracks after quality selec", 1);
    if (track1->Pt() > 20.){
      fMuonCounterHist->Fill("muon tracks after quality selec + pT>20", 1);
      // we want to know muon pt distributions after all cuts
      if (track1->Charge()==muonCharge){
	fMuPlusPtAllMuons->Fill(track1->Pt());
	numMuPlus++;
      }
      else if (track1->Charge()==-muonCharge){
	fMuMinusPtAllMuons->Fill(track1->Pt());
	numMuMin++;
      }
    }

    //cout << "single muon pT is " << track1->Pt() << " while single muon P is " << track1->P() << endl;

    fSingleMuonPt->Fill(ptMuon);
    fSingleMuonEta->Fill(track1->Eta());
    fSingleMuonPhi->Fill(track1->Phi());
    
    //we need this to calculate dimuon pT
    TLorentzVector lorentzVTrack1 = MuonTrackToLorentzVector(track1);
    
    // second track loop for dimuon analysis
    // note that the index starts at iTrack+1, to avoid overcounting
    for (Int_t iTrack2 = iTrack+1; iTrack2 < nTracksMuonUtil; iTrack2++) {
      AliVParticle *track2 = (AliVParticle*)AliAnalysisMuonUtility::GetTrack(iTrack2,fAODEvent);
      if (!track2) continue;
      if (!fMuonTrackCuts->IsSelected(track2)) continue;

      // same check as for track1 for MC reco
      if (bIsMC){
	// check if the particle is associated to an MC particle
	// if the track  has no associated particle, throw it away!
	if (track2->GetLabel() == -1) continue;
	
	// only if it did, check if the mother MC particle was a Z
	TClonesArray *mcarray =
	  dynamic_cast<TClonesArray*>(InputEvent()->FindListObject(AliAODMCParticle::StdBranchName()));
	if (mcarray){	
	  AliAODMCParticle* mcp = (AliAODMCParticle*)mcarray->At(track2->GetLabel());
	  // check that the muon has a correct status code of 1
	  // if not, throw it away!
	  if (mcp->MCStatusCode() != 1) continue;
	  
	  // check that the muon has a mother in the first place and if so, it is a Z
	  // if not, throw it away!
	  indexMother2 = mcp->GetMother();
	  if (indexMother2 == -1) continue;
	  // now test if mother/grandmother/GGmother/etc was a Z
	  Bool_t cameFromZ2 = kFALSE;
	  while (indexMother2 > -1){
	    AliAODMCParticle* motherMcp = (AliAODMCParticle*)mcarray->At(indexMother2);
	    if (motherMcp->PdgCode() == 23){
	      cameFromZ2 = kTRUE;
	      indexMother2 = -5; // ugly solution!
	    }
	    else{
	      indexMother2 = motherMcp->GetMother();
	    }
	  }
	  if (!cameFromZ2) continue;
	}
      }
      
      // now obtain fourvector of the second muon and sum it with that
      // of the first to obtain dimuon 4vector from which we extract pT
      TLorentzVector lorentzVTrack2 = MuonTrackToLorentzVector(track2);
      TLorentzVector lorentzVecSum = lorentzVTrack1 + lorentzVTrack2;
      Float_t mass = lorentzVecSum.M();
      Float_t dimuonPt = lorentzVecSum.Pt();
      Float_t dimuonPhi = lorentzVTrack1.Angle(lorentzVTrack2.Vect());
      Float_t dimuonY = lorentzVecSum.Rapidity();

      // we want to track the number of dimuon candidates per event
      // but only for unlike sign charges
      if (track2->Charge() != track1->Charge()){
	fDimuonCounterHist->Fill("opp.sign dimuon pairs", 1);
      }
      
      if (-4. < dimuonY && dimuonY < -2.5){
	if(track2->Charge() != track1->Charge()){
	  fInvMass->Fill(mass); // only fill rapidity in a selected range -2.5<rap<-4
	  f2DInvMassVsPtNoCuts->Fill(mass, dimuonPt);
	}

	// for Z boson analysis we have te requirement that Pt>20
	if (track1->Pt() > 20 && track2->Pt() > 20){
	  	  
	  // first look at same sign pairs
	  if (track2->Charge() == track1->Charge()){
	    fZBosonInvMassLS->Fill(mass);
	    fZBosonCentDistLS->Fill(centrality);
	  }

	  // before looking at unlike sign pairs in MC reco
	  // we want to make sure that the Z bosons come from the same Z!
	  if (bIsMC && !(indexMother1==indexMother2)) continue;
	  
	  // then we look at  unlike sign pairs
	  if (track2->Charge() != track1->Charge()){
	    fDimuonCounterHist->Fill("opp.sign dimuon pairs + Z cuts", 1);
	    fZBosonInvMass->Fill(mass);
	    fZBosonCentDist->Fill(centrality);

	    // dimuon eta phi and y distributions
	    fDimuonPhi->Fill(dimuonPhi);
	    fDimuonY->Fill(dimuonY);
	    fDimuonPt->Fill(dimuonPt);

	    // fill our THnSparseF object!
	    //order is centrality, mass, pt, Y!
	    Double_t sparseValue[4] = {centrality, mass, dimuonPt, dimuonY}; 
	    fZBosons->Fill(sparseValue);

	    // here we have our final candidates that also survived mass cut!
	    if (60. < mass && mass < 120.){
	      fZBosonY->Fill(dimuonY);
	      fZBosonPhi->Fill(dimuonPhi);
	      fZBosonPt->Fill(dimuonPt);
	      
	      // fill whichever track has positive value in the corresponding histograms
	      if (track1->Charge() == muonCharge){
		fMuPlusZBosonCandEta->Fill(track1->Eta(), mass);
		fMuPlusZBosonCandPt->Fill(track1->Pt(), mass);
		fMuPlusZBosonCandP->Fill(track1->P(), mass);
		fMuMinZBosonCandEta->Fill(track2->Eta(), mass);
		fMuMinZBosonCandPt->Fill(track2->Pt(), mass);
		fMuMinZBosonCandP->Fill(track2->P(), mass);
		fMuPlusMinusMuMinPt->Fill(track1->Pt() - track2->Pt(), mass);
		fMuPlusMinusMuMinP->Fill(track1->P() - track2->P(), mass);
		
	      }
	      else if (track2->Charge() == muonCharge){
		fMuPlusZBosonCandEta->Fill(track2->Eta(), mass);
		fMuPlusZBosonCandPt->Fill(track2->Pt(), mass);
		fMuPlusZBosonCandP->Fill(track2->P(), mass);
		fMuMinZBosonCandEta->Fill(track1->Eta(), mass);
		fMuMinZBosonCandPt->Fill(track1->Pt(), mass);
		fMuMinZBosonCandP->Fill(track1->P(), mass);
		fMuPlusMinusMuMinPt->Fill(track2->Pt() - track1->Pt(), mass);
		fMuPlusMinusMuMinP->Fill(track2->P() - track1->P(), mass);
	      }
	    }
	    
	    
	    // mass vs pt 2D hist
	    f2DMassVsPt->Fill(dimuonPt, mass);
	    
	    // also save the name of the file so that I can download it later!
	    // only in case of no MC obviously..
	    if (!bIsMC){
	      fZBosonFileNames->Fill(CurrentFileName(), 1);
	    }
	  }
	}
      }
    }
  }
  
  // only fill if there was at least 1 muon with more than 20 GeV/c!
  if (numMuPlus > 0 || numMuMin > 0){
    f2DMuonChargeDist->Fill(numMuPlus, numMuMin);
  }
  
  //////////////////////// MC ///////////////////////////////////////
  ///////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////
  // the following pieces of code will only be ran if the MC flag is
  // set to kTRUE, meaning we are running on MC data

  if (bIsMC){
    // we want to classify monte carlo events!
    // good event = event with at least 1 Z boson with inv mass in ROI
    // useless event = event with at least 1 Z, but outside of ROI
    // very useless event = event with no Z at all (doesn't need separate boolean)!
    Bool_t kGoodEvent = kFALSE;
    Bool_t kUselessEvent = kFALSE;
    
    // first obtain the array of MC particles
    TClonesArray *mcarray =
      dynamic_cast<TClonesArray*>(InputEvent()->FindListObject(AliAODMCParticle::StdBranchName()));
    if (mcarray){
      
      Int_t MCSize = mcarray->GetEntries();  
      // to loop over it, we need to know the size of the array first
      for(int i=0;i<MCSize;i++){
	AliAODMCParticle* mcp = (AliAODMCParticle*)mcarray->At(i);

	if( !mcp ) continue;

	// check on Z boson numbers
	if (mcp->PdgCode() == 23){
	  fMCZBosonCounter->Fill("all gen Z", 1);

	  //apply rapidity cut
	  if(mcp->Y() < -4 || mcp->Y() > -2.5) continue;
	  fMCZBosonCounter->Fill("+ rap. cut", 1);

	  // assume first that both decay muons are within the right kinematic range
	  // and if it turns out they're not, switch the boolean
	  Bool_t decayDaughterCuts = kTRUE;

	  Int_t numDaughters = mcp->GetNDaughters();
	  if (numDaughters > 1){
	    Int_t labelFirstDaughter = mcp->GetDaughterFirst();
	    //small loop to obtain the daughters
	    for (Int_t k=0; k<numDaughters;k++){
	      // start counting from the first daughter
	      // because the built in methods suck when there are more than 2 daughers
	      Int_t label = labelFirstDaughter + k;
	      AliAODMCParticle* daughter = (AliAODMCParticle*)mcarray->At(label);
	      // get the particle parameters first
	      Double_t eta = daughter->Eta();
	      Double_t theta = daughter->Theta();
	      Double_t thetaAbsDeg = theta * 180 / TMath::Pi();
	      Double_t pT = daughter->Pt();
	      Int_t PdgCode = daughter->PdgCode();
	      
	      
	      // check if particle is a muon. If not, don't bother to check the rest 
	      if (TMath::Abs(PdgCode) != 13) continue;
	      // eta cut
	      Bool_t EtaCut = (-4. < eta && eta < -2.5);
	      // theta abs cut. For this we need to convert first into degrees.
	      Bool_t ThetaCut = (170 < thetaAbsDeg && thetaAbsDeg < 178);
	      Bool_t pTCut = pT > 20.;
	      Bool_t survivedSelection = (EtaCut && ThetaCut && pTCut);
	      if (!survivedSelection) decayDaughterCuts = kFALSE;
	    }
	  }
	  // by now we've confirmed that the Z boson decays into a muon pair
	  // that falls within the right cuts we apply
	  if (decayDaughterCuts) continue;
	  fMCZBosonCounter->Fill("+ daughter cuts", 1);

	  Double_t mass = mcp->GetCalcMass();
	  if (60. < mass && mass < 120.) fMCZBosonCounter->Fill("+ inv Mass in ROI", 1);
	}

	// single muon cuts on the MC particles
	if (! survivedCutsMCParticles(mcp)) continue;
	
	// fill positive and negative muons separately
	// note that PDG code 13 corresponds to mu- while
	// -13 corresponds to mu+!!
	// also don't use mcp->Charge() as that gives you charge in units
	// of |e|/3, which is very confusing!
	if(mcp->PdgCode() == -13){
	  fMCMuPlusEta->Fill(mcp->Eta());
	  fMCMuPlusTheta->Fill(mcp->Theta() * TMath::RadToDeg());
	  fMCMuPlusPt->Fill(mcp->Pt());
	  fMCMuPlusP->Fill(mcp->P());
	}
	else if(mcp->PdgCode() == 13){
	  fMCMuMinEta->Fill(mcp->Eta());
	  fMCMuMinTheta->Fill(mcp->Theta() * TMath::RadToDeg());
	  fMCMuMinPt->Fill(mcp->Pt());
	  fMCMuMinP->Fill(mcp->P());
	}
	
	// dimuon loop for muon pairs
	TLorentzVector MCLorentzVec1;
	TLorentzVector MCLorentzVec2;
	TLorentzVector MCLorentzVecSum;
	Int_t indexMother;
	AliAODMCParticle* motherMCparticle;
	Int_t iMother1, iMother2;
	Bool_t bMCCameFromZ1, bMCCameFromZ2;
	bMCCameFromZ1 = kFALSE;
	bMCCameFromZ2 = kFALSE;
	Int_t iZMother1, iZMother2;

	// check if we have a muon
	if(mcp->PdgCode() == 13 || mcp->PdgCode() ==-13){
	  
	  // now check if the mother was a zboson and retrieve index of Z mother
	  iMother1 = mcp->GetMother();
	  
	  while (iMother1 > -1){
	    motherMCparticle = (AliAODMCParticle*)mcarray->At(iMother1);
	    if (motherMCparticle->PdgCode() == 23) {
	      bMCCameFromZ1 = kTRUE;
	      iZMother1 = iMother1;
	      iMother1 = -5; // ugly solution!
	      //save the Z boson mother index 
	      //should go on with the loop
	    }
	    else{
	      iMother1 = motherMCparticle->GetMother();
	    }
	  }
	    
	  if (bMCCameFromZ1){
	    for(Int_t j=i+1;j<MCSize;j++){
	      AliAODMCParticle* mcp2 = (AliAODMCParticle*)mcarray->At(j);
	      if (!mcp2) continue;
	      
	      // check if mcp2 falls within the track cuts
	      if (! survivedCutsMCParticles(mcp2)) continue;
	      
	      ///check that particle 2 came from Z boson and whether it has same mother
	      // now check if the mother was a zboson and retrieve index of Z mother
	      iMother2 = mcp->GetMother();
	      
	      while (iMother2 > -1){
		motherMCparticle = (AliAODMCParticle*)mcarray->At(iMother2);
		if (motherMCparticle->PdgCode() == 23) {
		  bMCCameFromZ2 = kTRUE;
		  iZMother2 = iMother2;
		  iMother2 = -5; // ugly solution!
		  //save the Z boson mother index 
		  //should go on with the loop
		}
		else{
		  iMother2 = motherMCparticle->GetMother();
		}
	      }
	      
	      if (bMCCameFromZ2){
		// check if pT > 20 for both muons
		if (mcp->Pt() < 20. || mcp2->Pt() < 20.) continue;
		
		// check for opposite sign dimuons that come from the same mother
		if (mcp2->PdgCode() == -mcp->PdgCode() && iZMother1 == iZMother2){
		  
		  // by now we know that we have a dimuon pair which originated from the same Z boson

		  // we want to compare the Z boson properties between the generated mother
		  // and that of the dimuon pair of the final detected muons (maybe differences due to ISR/FSR)

		  AliAODMCParticle* mcpZ = (AliAODMCParticle*)mcarray->At(iZMother1);
		  if (-4 < mcpZ->Y() && mcpZ->Y() < -2.5){
		    //order is centrality, mass, pt, Y!
		    Double_t sparseValuePureZ[4] = {centrality, mcpZ->GetCalcMass(), mcpZ->Pt(), mcpZ->Y()}; 
		    fMCPureZBosons->Fill(sparseValuePureZ);
		  }
		  
		  
		  MCLorentzVec1 = MuonTrackToLorentzVector(mcp);
		  MCLorentzVec2 = MuonTrackToLorentzVector(mcp2);
		  MCLorentzVecSum = MCLorentzVec1 + MCLorentzVec2;
		  
		  // set the flags for the event quality (good/useless/very useless)
		  Float_t ZInvMass = MCLorentzVecSum.M();
		  Float_t ZRapidity = MCLorentzVecSum.Rapidity();
		  Float_t ZPt = MCLorentzVecSum.Pt();
		  Float_t ZEnergy = MCLorentzVecSum.E();

		  /*
		  // do a check that dimuon is same as taking the Z mother mcp itself
		  AliAODMCParticle* mcpZ = (AliAODMCParticle*)mcarray->At(iZMother1);
		  cout << "mclorentzvec mass " << ZInvMass << endl;
		  cout << "mrlorentzvec energy " << ZEnergy << endl;
		  cout << "zboson mass " << mcpZ->GetCalcMass() << endl;
		  cout << "zboson energy is " << mcpZ->E() << endl;

		  cout << "printing z boson " << endl;
		  mcpZ->Print();

		  Int_t iZbosonMother = mcpZ->GetMother();
		  AliAODMCParticle* mcpZMother = (AliAODMCParticle*)mcarray->At(iZbosonMother);
		  cout << "printing mother " << endl;
		  mcpZMother->Print();
		  cout << "mother mass, energy and pt are " << mcpZMother->GetCalcMass() << " "
		       << mcpZMother->E() << " " << mcpZMother->Pt() << endl;

		  Int_t iZbosonGrandMother = mcpZMother->GetMother();
		  AliAODMCParticle* mcpZGrandMother = (AliAODMCParticle*)mcarray->At(iZbosonGrandMother);
		  cout << "printing grandmother " << endl;
		  mcpZGrandMother->Print();
		  cout << "grandmother mass, energy and pt are " << mcpZGrandMother->GetCalcMass() <<
		    " " << mcpZGrandMother->E() << " " << mcpZGrandMother->Pt() << endl;

		  Int_t iZbosonGrandGrandMother = mcpZGrandMother->GetMother();
		  AliAODMCParticle* mcpZGrandGrandMother = (AliAODMCParticle*)mcarray->At(iZbosonGrandGrandMother);
		  cout << "printing grandgrandmother " << endl;
		  mcpZGrandGrandMother->Print();
		  cout << "grandgrandmother mass, energy and pt are " << mcpZGrandGrandMother->GetCalcMass() <<
		    " " << mcpZGrandGrandMother->E() << " " << mcpZGrandGrandMother->Pt() << endl;

		  Int_t iZbosonGrandGrandGrandMother = mcpZGrandGrandMother->GetMother();
		  AliAODMCParticle* mcpZGrandGrandGrandMother = (AliAODMCParticle*)mcarray->At(iZbosonGrandGrandGrandMother);
		  cout << "printing grandgrandgrandmother " << endl;
		  mcpZGrandGrandGrandMother->Print();
		  cout << "grandgrandgrandmother mass, energy and pt are " << mcpZGrandGrandGrandMother->GetCalcMass() <<
		    " " << mcpZGrandGrandGrandMother->E() << " " << mcpZGrandGrandGrandMother->Pt() << endl;	      					  		  
		  Int_t iDaughter1;
		  Int_t iDaughter2;

		  iDaughter1 = mcpZ->GetDaughterFirst();
		  iDaughter2 = mcpZ->GetDaughterLast();

		  AliAODMCParticle* mcpDaughter1 = (AliAODMCParticle*)mcarray->At(iDaughter1);
		  cout << "printing daughter 1" << endl;
		  mcpDaughter1->Print();
		  cout << "daughter 1 energy " << mcpDaughter1->E() << endl;
		  AliAODMCParticle* mcpDaughter2 = (AliAODMCParticle*)mcarray->At(iDaughter2);
		  cout << "printing daughter 2" << endl;
		  mcpDaughter2->Print();
		  cout << "daughter 2 energy " << mcpDaughter2->E() << endl;

		  Int_t iGrandDaughter1;
		  Int_t iGrandDaughter2;

		  iGrandDaughter1 = mcpDaughter1->GetDaughterFirst();
		  iGrandDaughter2 = mcpDaughter2->GetDaughterFirst();

		  AliAODMCParticle* mcpGrandDaughter1 = (AliAODMCParticle*)mcarray->At(iGrandDaughter1);
		  cout << "printing granddaughter 1" << endl;
		  mcpGrandDaughter1->Print();
		  cout << "granddaughter 1 energy " << mcpGrandDaughter1->E() << endl;
		  AliAODMCParticle* mcpGrandDaughter2 = (AliAODMCParticle*)mcarray->At(iGrandDaughter2);
		  cout << "printing granddaughter 2" << endl;
		  mcpGrandDaughter2->Print();
		  cout << "granddaughter 2 energy " << mcpGrandDaughter2->E() << endl;
		  
		  cout << "in conclusion " << endl;
		  cout << "Z Boson mass, energy pt " << mcpZ->GetCalcMass() << " " << mcpZ->E() << " " << mcpZ->Pt() << endl;

		  
		  TLorentzVector TLorentzVecDaughter1 = MuonTrackToLorentzVector(mcpDaughter1);
		  TLorentzVector TLorentzVecDaughter2 = MuonTrackToLorentzVector(mcpDaughter2);
		  TLorentzVector SumDaughters = TLorentzVecDaughter1 + TLorentzVecDaughter2;
		  cout << "dimuon daughters mass energy pt " << SumDaughters.M() << " " << SumDaughters.E() << " " << SumDaughters.Pt() << endl;
		  cout << "dimuon granddaughters mass energy pt " << MCLorentzVecSum.M() << " " << MCLorentzVecSum.E() << " " << MCLorentzVecSum.Pt() << endl;

		  cout << "sanity check: granddaughters must correspond to mcp and mcp2 " << endl;
		  mcp->Print();
		  mcp2->Print();
		  */
		  
		  // fill one inv mass spectrum before doing rapidity and mass cuts
		  fMCDimuInvMassBeforeCuts->Fill(ZInvMass);
		  
		  Bool_t kDimuRapidityCut = (-4 < ZRapidity && ZRapidity < -2.5);
		  Bool_t kDimuMassCut = (60. < ZInvMass && ZInvMass < 120.);
		  
		  if (kDimuRapidityCut && kDimuMassCut) kGoodEvent = kTRUE;
		  else kUselessEvent = kTRUE;
		  
		  // now that we have the dimuon object, let's fill the histograms		  
		  // the mass cut is now NOT applied, so that we can study this offline and
		  // make the cut later
		  if (kDimuRapidityCut){
		    fMCDimuY->Fill(MCLorentzVecSum.Rapidity());
		    fMCDimuPt->Fill(MCLorentzVecSum.Pt());
		    fMCDimuP->Fill(MCLorentzVecSum.P());
		    fMCDimuInvMass->Fill(MCLorentzVecSum.M());
		    fMCDimuAngle->Fill(MCLorentzVec1.Angle(MCLorentzVec2.Vect()));

		    // fill the THnSparse with so far only the cut on rapidity
		    //order is centrality, mass, pt, Y!
		    Double_t sparseValue[4] = {centrality, MCLorentzVecSum.M(), MCLorentzVecSum.Pt(), MCLorentzVecSum.Rapidity()}; 
		    fMCZBosons->Fill(sparseValue);
		    		    
		    // here we can fill all the 2d histograms for mass vs pt
		    fMC2DMassVsPt->Fill(ZPt, ZInvMass);
		  }
		}
	      }
	    }
	  }
	}
      }
    }
    
    // now fill the histogram for event quality
    // GOOD = TRUE, USELESS TRUE : GOOD
    // GOOD = TRUE, USELESS = FALSE : GOOD
    // GOOD = FALSE, USELESS = TRUE : USELESS
    // GOOD = FALSE, USELESS = FALSE : VERY USELESS
    if (kGoodEvent) fMCGoodEventCounter->Fill("good event", 1);
    else if (kUselessEvent) fMCGoodEventCounter->Fill("useless event", 1);
    else fMCGoodEventCounter->Fill("v. useless event", 1);
  }
  
  // Required both here and in UserCreateOutputObjects()
  PostData(1, fOutputList);
}

//___________________________________________________________________________
void AliAnalysisTaskZBoson::Terminate(Option_t *) 
{
  /*
  // we use alphanumeric labels for showing this plot, so we want to have it in ascending order on number
  fMCZBosonDaughters->LabelsOption("a", "X");
  fMCVersion->LabelsOption("a", "X");
  
  //cout << "printing out some event info " << endl;
  //cout << fMuonEventCuts->GetVertexVzMin() << endl;
  //cout << fMuonEventCuts->GetVertexVzMax() << endl;
  fMuonEventCuts->Print();
  
  if (fEventCounters){
    fEventCounters->Print("trigger");
    fEventCounters->Print();
    //Double_t test1 = fEventCounters->GetSum("trigger:ANY");
    //cout << test1 << endl;
  }
  //fMuonTrackCuts->Print();
  */  
}

TLorentzVector AliAnalysisTaskZBoson::MuonTrackToLorentzVector(const TObject *obj)
{   
    Float_t muonMass = 0.105658369;     
    TLorentzVector lvMuon;                 
 
    const AliVParticle* muonTrack = static_cast<const AliVParticle*> ( obj );      
    Float_t energy = muonMass*muonMass + muonTrack->P()*muonTrack->P();            
    if (energy>0) energy = TMath::Sqrt(energy);                                    
    else energy = -1;                   
    lvMuon.SetPxPyPzE(muonTrack->Px(), muonTrack->Py(), muonTrack->Pz(), energy);  

    return lvMuon;
}

// function that checks if an MC particle is a muon and
// cuts on eta and theta
Bool_t survivedCutsMCParticles(AliAODMCParticle* MCParticle){
  // get the particle parameters first
  Double_t eta = MCParticle->Eta();
  Double_t theta = MCParticle->Theta();
  Double_t thetaAbsDeg = theta * 180 / TMath::Pi();
  Int_t PdgCode = MCParticle->PdgCode();
  Int_t StatusCode = MCParticle->MCStatusCode();

  // check if particle is a muon
  Bool_t isMuon;
  if (TMath::Abs(PdgCode) == 13) isMuon = kTRUE;

  // select on final state muons
  Bool_t isCorrectStatusCode = kFALSE;
  if (StatusCode == 1) isCorrectStatusCode = kTRUE;
  
  // eta cut
  Bool_t EtaCut = (-4. < eta && eta < -2.5);

  // theta abs cut. For this we need to convert first into degrees.
  Bool_t ThetaCut = (170 < thetaAbsDeg && thetaAbsDeg < 178);

  Bool_t survivedSelection = (isMuon && EtaCut && ThetaCut && isCorrectStatusCode);
  return survivedSelection;
}
